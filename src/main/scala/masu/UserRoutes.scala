package masu

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging

import scala.concurrent.duration._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.delete
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.directives.PathDirectives.path

import scala.concurrent.Future
import masu.UserRegistryActor._
import akka.pattern.ask
import akka.util.Timeout

//#user-routes-class
trait UserRoutes extends JsonSupport {

  // we leave these abstract, since they will be provided by the App
  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[UserRoutes])

  // other dependencies that UserRoutes use
  def userRegistryActor: ActorRef

  // Required by the `ask` (?) method below
  implicit lazy val timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration

  //#all-routes
  //#users-get-post
  //#users-get-delete   
  lazy val userRoutes: Route =
        post {
          entity(as[RequestData]){data =>{
            (userRegistryActor ? HandleRequest(data))
            complete(StatusCodes.OK, "Accepted")
          }
          }
//              entity(as[User]) { user =>
//                val userCreated: Future[ActionPerformed] =
//                  (userRegistryActor ? CreateUser(user)).mapTo[ActionPerformed]
//                onSuccess(userCreated) { performed =>
//                  log.info("Created user [{}]: {}", user.name, performed.description)
//                  complete((StatusCodes.Created, performed))
//                }
//              }
        }
      //#users-get-delete

  //#all-routes
}
