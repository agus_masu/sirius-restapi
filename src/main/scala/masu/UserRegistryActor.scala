package masu

//#user-registry-actor
import java.math.BigInteger
import java.util.Base64

import akka.actor.{Actor, ActorLogging, Props}

//#user-case-classes
final case class RequestData(deveui: String, timestamp: String, dataFrame: String)
final case class HandleRequest(data: RequestData)
final case class FullData(deveui: String, timestamp: String, fullData: String)

final case class FinalUser(familyID: Int, typeID: Int, batteryLevel: Int, timeStamp: Long, cv: Long, ucv: Long, uufv: Long, tat: Int)

final case class ReceivingData(deveui: String, timestamp: String, dataFrames: Array[String])
//#user-case-classes

object UserRegistryActor {

  def props: Props = Props[UserRegistryActor]
}

class UserRegistryActor extends Actor with ActorLogging {

  var onReceiving: List[ReceivingData] = Nil

  import UserRegistryActor._

  //var users = Set.empty[User]

  def receive: Receive = {
    case HandleRequest(data) => handleRequest(data);
  }

  private def handleRequest(data: RequestData): Unit = {
    //Decodes base64 data
    val streamBlock: String = getDecodeFrameMessage(data.dataFrame)

    //Get data from Hexa Block
    val order: Int = Character.digit(streamBlock.charAt(0), 16);
    val total: Int = Character.digit(streamBlock.charAt(1), 16);
    val dataFrame: String = streamBlock.substring(2);

    println("Order " + order)
    println("TOtal: " + total)

    //Tests if Data already exists
    getReceivingData(data.deveui) match {
      case None => {
        println("New Data Receiving")
        var newData: ReceivingData = ReceivingData(data.deveui, data.timestamp, new Array[String](total))
        newData.dataFrames(order-1) = dataFrame;
        onReceiving = newData :: onReceiving
        testCompleteData(newData)
      }
      case Some(d) => {
        println("Data already exists")
        if (d.dataFrames(order-1) != null) return;
        d.dataFrames(order-1) = dataFrame
        testCompleteData(d)
      }
    }
  }

  private def getReceivingData(deveui: String): Option[ReceivingData] = onReceiving.find(_.deveui.equals(deveui));

  private def getDecodeFrameMessage(encodedFramePack: String): String = String.format("%02X", new BigInteger(1, Base64.getDecoder.decode(encodedFramePack)))

  private def testCompleteData(data: ReceivingData): Unit = {
    var fullMsg: String = "";
    for (s <- data.dataFrames){
      if (s == null) return;
      fullMsg = fullMsg + s;
    }
    println(fullMsg);
    println("Length: " + fullMsg.length / 2)

    val fullData: FullData = FullData(data.deveui, data.timestamp, fullMsg)
    generateFinalClass(fullData)

    //Deletes Full Data from Receiving List
    onReceiving = onReceiving.filter(!_.deveui.eq(data.deveui))
  }

  private def generateFinalClass (data: FullData) : Unit = {
    val fullData: String = data.fullData;
    val familyID: Int = Integer.parseInt(fullData.substring(0,2), 16)
    val typeID: Int = Integer.parseInt(fullData.substring(2,4), 16)

    val batteryLevelInput: Int = Integer.parseInt(fullData.substring(3,5), 16)
    val batteryLevel: Int = (batteryLevelInput * 100);

    val timestamp: Long = Long2long(java.lang.Long.valueOf(fullData.substring(5,13), 16))
    val cv: Long = Long2long(java.lang.Long.valueOf(fullData.substring(13,21), 16))
    val ucv: Long = Long2long(java.lang.Long.valueOf(fullData.substring(21,29), 16))
    val uufv: Long = Long2long(java.lang.Long.valueOf(fullData.substring(29,37), 16))
    val tat: Int = Integer.parseInt(fullData.substring(37 , 41), 16)


    val finalUser: FinalUser = FinalUser(familyID, typeID, batteryLevel, timestamp, cv, ucv, uufv, tat)
    println(finalUser.toString)
  }

}
//#user-registry-actor